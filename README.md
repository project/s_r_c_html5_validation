SELECT RADIO CHECKBOX FIELD HTML 5 VALIDATION
---------------------------

INTRODUCTION
-----------
  Providing an HTML 5 Validation on Select Radio Checkbox Field.

INSTALLATION:
-------------
  1. Install as you would normally install a contributed Drupal module. 
     See: https://www.drupal.org/node/895232 for further information.

REQUIREMENTS
------------
  The basic Select Radio Checkbox field HTML 5 validation module has 
  forms dependencies, nothing special is required.

UNINSTALLATION
--------------
  1. Uninstall as you would normally Uninstall a contributed Drupal module. 
     See: https://www.drupal.org/docs/user_guide/en/config-uninstall.html 
     for further information.
